# butacaventura_system



## Explicaciòn de los sistemas.

# Sign up

El frontend envia una verificacion al correo del usuario en caso, de que no haya elegido el username, luego envía una petición post al microservicio /server/users/signup para enviar la información requerida a la base de datos a través del body (Nombre, Apellido, etc.). Luego, dependiendo de la situación, el backend responde al frontend con un mensaje que indica lo obtenido de la petición.

# Login

El frontend envía el nombre de usuario y la contraseña como parámetros al microservicio /server/user/:username/:password a través de una petición GET. El microservicio se comunica con la base de datos para verificar si esos dos parámetros existen en la base de datos. Dependiendo de esto, el servidor enviará la respuesta correspondiente al frontend.


